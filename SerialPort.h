#pragma once

#include <string>

#if defined(WIN32)
#include <windows.h>
#endif

enum class SerialPortParity {
	SPP_NO_PARITY,
	SPP_EVEN,
	SPP_ODD
};

class SerialPort {
private:
#if defined(WIN32)
	HANDLE 	m_Handler;
	COMSTAT m_Status{};
	DWORD 	m_Errors{};
#endif
#if defined(unix) || defined(__unix__) || defined(__unix)
	int 	m_SerialPort;
#endif
	bool 		m_Connected;
	std::string m_PortName;
public:
	/**
	 * Object to communicate in serial on COM ports
	 * @param portName name of the COM port
	 * @param timeout_ms timeout in millisecond
	 * @param baudRate baudrate
	 * @param bits number of data bits
	 * @param stopBits number of stop bits
	 * @param parity parity
	 * @example SerialPort("COM7", 1000, 9600, 8, 1, SPP_NO_PARITY, false)
	 */
	SerialPort(const std::string &portName,
			   unsigned int timeout_ms,
			   int baudRate,
			   int bits,
			   int stopBits,
			   SerialPortParity parity,
			   bool flowControl);
	~SerialPort();

	/**
	 * Reads n bytes into buffer
	 * @param buffer data buffer
	 * @param n number of bytes to be read
	 * @return -1 if read failed, number of bytes read otherwise
	 */
	int    readBytes(unsigned char *buffer, int n);

	/**
	 * Reads one byte into buffer
	 * @param buffer data buffer
	 * @return -1 if read failed, number of bytes read otherwise
	 */
	int    readByte(unsigned char *buffer);

	/**
	 * Writes n bytes from buffer
	 * @param buffer data buffer
	 * @param n number of bytes to write
	 * @return -1 if write operation failed, number of bytes written otherwise
	 */
	int    writeBytes(unsigned char *buffer, int n);

	/**
	 * Writes one byte from buffer
	 * @param buffer data buffer
	 * @return -1 if write operation failed, number of bytes written otherwise
	 */
	int    writeByte(unsigned char *buffer);

	/**
	 * Return the COM connection state
	 * @return true if the connection to the port has been established
	 */
	bool 		isConnected() const;

	/**
	 * @return name of the COM port used
	 */
	std::string getPortName() const;
};
