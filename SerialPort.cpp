#include "SerialPort.h"
#include "Exception.h"

#if defined(unix) || defined(__unix__) || defined(__unix)
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <cassert>
#include <asm/ioctls.h>
#include <asm/termbits.h>
#include <unistd.h>
#endif

SerialPort::SerialPort(const std::string &portName, unsigned int timeout_ms, int baudRate, int bits, int stopBits, SerialPortParity parity, bool flowControl): 	m_Connected(false), m_PortName(portName) {

	if (bits > 8)
		bits = 8;
	else if (bits < 5)
		bits = 5;

#if defined(WIN32)
	m_Handler = CreateFileA(static_cast<LPCSTR>(portName.c_str()),
							GENERIC_READ | GENERIC_WRITE,
							0,
							nullptr,
							OPEN_EXISTING,
							FILE_ATTRIBUTE_NORMAL,
							nullptr);
	if (m_Handler == INVALID_HANDLE_VALUE){
		if (GetLastError() == ERROR_FILE_NOT_FOUND) {
			std::string tmp_str = "Handle was not attached. Reason: ";
			tmp_str+= portName;
			tmp_str+= " not available";
			THROW_EXCEPTION(tmp_str);
		} else {
			THROW_EXCEPTION("Invalid handle value");
		}
	}
	else {
		DCB dcbSerialParameters = {0};

		if (!GetCommState(m_Handler, &dcbSerialParameters)) {
			THROW_EXCEPTION("Failed to get comm state");
		}
		else {
			dcbSerialParameters.BaudRate = baudRate;
			dcbSerialParameters.ByteSize = bits;

			switch (stopBits) {
				case 1:
					dcbSerialParameters.StopBits = ONESTOPBIT;
					break;
				case 2:
					dcbSerialParameters.StopBits = TWOSTOPBITS;
					break;
				default:
					dcbSerialParameters.StopBits = ONESTOPBIT;
			}

			if (flowControl)
				dcbSerialParameters.fDtrControl = DTR_CONTROL_ENABLE;
			else
				dcbSerialParameters.fDtrControl = DTR_CONTROL_DISABLE;

			switch (parity) {
				case SerialPortParity::SPP_NO_PARITY:
					dcbSerialParameters.Parity = NOPARITY;
					break;
				case SerialPortParity::SPP_EVEN:
					dcbSerialParameters.Parity = EVENPARITY;
					break;
				case SerialPortParity::SPP_ODD:
					dcbSerialParameters.Parity = ODDPARITY;
					break;
			}

			if (!SetCommState(m_Handler, &dcbSerialParameters)) {
				THROW_EXCEPTION("Could not set comm parameters");
			} else {
				m_Connected = true;
				PurgeComm(m_Handler, PURGE_RXCLEAR | PURGE_TXCLEAR);
			}

			COMMTIMEOUTS timeouts = {0};
			if(GetCommTimeouts(m_Handler, &timeouts)) {
				timeouts.ReadTotalTimeoutConstant = timeout_ms;
			}
			else
				THROW_EXCEPTION("Could not get timeout parameters");

			if(!SetCommTimeouts(m_Handler, &timeouts)) {
				THROW_EXCEPTION("Could not set timeout parameters");
			}
		}
	}
#endif
#if defined(unix) || defined(__unix__) || defined(__unix)
	m_SerialPort = open(portName.c_str(), O_RDWR);

	if (m_SerialPort < 0) {
		std::string tmp_str = "Could not open Port: ";
		tmp_str+= portName;
		THROW_EXCEPTION(tmp_str);
	}

    struct termios2 tty;
    ioctl(m_SerialPort, TCGETS2, &tty);

	switch (parity) {
		case SerialPortParity::SPP_NO_PARITY:
			tty.c_cflag &= ~PARENB;
			break;
		case SerialPortParity::SPP_ODD:
			tty.c_cflag |= PARENB;
			tty.c_cflag |= PARODD;
			break;
		case SerialPortParity::SPP_EVEN:
			tty.c_cflag |= PARENB;
			tty.c_cflag &= ~PARODD;
			break;
	}

	switch (stopBits) {
		case 2:
			tty.c_cflag |= CSTOPB;
			break;
		default:
			tty.c_cflag &= ~CSTOPB;
	}

	tty.c_cflag &= ~CSIZE;
	switch (bits) {
		case 5:
			tty.c_cflag |= CS5;
			break;
		case 6:
			tty.c_cflag |= CS6;
			break;
		case 7:
			tty.c_cflag |= CS7;
			break;
		case 8:
			tty.c_cflag |= CS8;
			break;
	}

	if (flowControl)
		tty.c_cflag |= CRTSCTS;
	else
		tty.c_cflag &= ~CRTSCTS;

	tty.c_cflag |= CREAD | CLOCAL;
    tty.c_lflag	&= ~ICANON;
    tty.c_lflag &= ~ECHO;
    tty.c_lflag	&= ~ECHOE;
    tty.c_lflag	&= ~ECHONL;
    tty.c_lflag	&= ~ISIG;
	tty.c_iflag &= ~(IXON | IXOFF | IXANY);
	tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL);
    tty.c_oflag =   0;
    tty.c_oflag &=  ~OPOST;
    tty.c_cc[VTIME] = (cc_t)(timeout_ms/100);
    tty.c_cc[VMIN] = 0;
    tty.c_cflag &= ~CBAUD;
    tty.c_cflag |= CBAUDEX;
    tty.c_ispeed = baudRate;
    tty.c_ospeed = baudRate;

	m_Connected = true;
#endif
}

SerialPort::~SerialPort() {
	if (m_Connected){
		m_Connected = false;

#if defined(WIN32)
		CloseHandle(m_Handler);
#endif
#if defined(unix) || defined(__unix__) || defined(__unix)
		if (close(m_SerialPort) != 0) {
			THROW_EXCEPTION("Could not close serial port");
		}
#endif
	}
}

int SerialPort::readBytes(unsigned char *buffer, int buf_size) {
#if defined(WIN32)
	unsigned long bytesRead;
	ClearCommError(m_Handler, &m_Errors, &m_Status);

	if (ReadFile(m_Handler, buffer, buf_size, (&bytesRead), nullptr)) {
		return (int)bytesRead;
	}
	return -1;
#endif
#if defined(unix) || defined(__unix__) || defined(__unix)
	return read(m_SerialPort, buffer, buf_size);
#endif
}

int SerialPort::readByte(unsigned char *buffer) {
	return readBytes(buffer, 1);
}

int SerialPort::writeBytes(unsigned char *buffer, int buf_size) {
#if defined(WIN32)
	DWORD bytesSend;
	if (!WriteFile(m_Handler, (void*) buffer, buf_size, &bytesSend, 0)) {
		ClearCommError(m_Handler, &m_Errors, &m_Status);
		return -1;
	}
	else
		return (int)bytesSend;
#endif
#if defined(unix) || defined(__unix__) || defined(__unix)
	return write(m_SerialPort, buffer, buf_size);
#endif

}

int SerialPort::writeByte(unsigned char *buffer) {
	return writeBytes(buffer, 1);
}

bool SerialPort::isConnected() const {
	return m_Connected;
}

std::string SerialPort::getPortName() const {
	return m_PortName;
}